// Import thư viện express
const express = require("express");

const router = express.Router();

// Import course middleware sang
const courseMiddleware = require("../middlewares/course.middleware");

// router.use(courseMiddleware.courseRouterMiddleware);

// Get all
router.get("/courses", courseMiddleware.courseGetAllMiddleware, (req, res) => {
    res.status(200).json({
        message: "Get all course"
    })
});

// Create
router.post("/courses", courseMiddleware.courseCreateMiddleware, (req, res) => {
    res.status(200).json({
        message: "Create course"
    })
});

// Get detail
router.get("/courses/:courseid", courseMiddleware.courseGetDetailMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    res.status(200).json({
        message: "Get course detail id = " + courseid
    })
});

// Update
router.put("/courses/:courseid", courseMiddleware.courseUpdateMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    res.status(200).json({
        message: "Update course id = " + courseid
    })
});

// Delete
router.delete("/courses/:courseid", courseMiddleware.courseDeleteMiddleware, (req, res) => {
    const courseid = req.params.courseid;

    res.status(200).json({
        message: "Delete course id = " + courseid
    })
});

// Export router thành 1 module
module.exports = router;