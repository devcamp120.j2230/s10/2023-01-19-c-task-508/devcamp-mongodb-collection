const reviewRouterMiddleware = (req, res, next) => {
    console.log("Review Request URL: ", req.url);
    
    next();
}

const reviewGetAllMiddleware = (req, res, next) => {
    console.log("Review Get ALL Middleware");
    
    next();
}

const reviewGetDetailMiddleware = (req, res, next) => {
    console.log("Review Get Detail Middleware");
    
    next();
}

const reviewCreateMiddleware = (req, res, next) => {
    console.log("Review Create Middleware");
    
    next();
}

const reviewUpdateMiddleware = (req, res, next) => {
    console.log("Review Update Middleware");
    
    next();
}

const reviewDeleteMiddleware = (req, res, next) => {
    console.log("Review Delete Middleware");
    
    next();
}

module.exports = {
    reviewRouterMiddleware,
    reviewGetAllMiddleware,
    reviewCreateMiddleware,
    reviewGetDetailMiddleware,
    reviewUpdateMiddleware,
    reviewDeleteMiddleware
}